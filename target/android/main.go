//go:build darwin || linux || windows
// +build darwin linux windows

package main

import (
	game "gitlab.com/rigel314/go-puzzlefrenzy"
	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/exp/app/debug"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/gl"
)

var (
	images *glutil.Images
	fps    *debug.FPS
)

func main() {
	g := new(game.Game)
	// g.Defaults()
	app.Main(func(a app.App) {
		var glctx gl.Context
		var sz size.Event
		for e := range a.Events() {
			switch e := a.Filter(e).(type) {
			case lifecycle.Event:
				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					glctx, _ = e.DrawContext.(gl.Context)
					g.InitContext(glctx)
					images = glutil.NewImages(glctx)
					fps = debug.NewFPS(images)
					a.Send(paint.Event{})
				case lifecycle.CrossOff:
					g.DestroyContext()
					fps.Release()
					images.Release()
					glctx = nil
				}
			case size.Event:
				sz = e
				g.SizeEvent(sz.WidthPx, sz.HeightPx, sz)
			case paint.Event:
				if glctx == nil || e.External {
					// As we are actively painting as fast as
					// we can (usually 60 FPS), skip any paint
					// events sent by the system.
					continue
				}

				g.Paint()
				fps.Draw(sz)
				a.Publish()
				// Drive the animation by preparing to paint the next frame
				// after this one is shown.
				a.Send(paint.Event{})
			case touch.Event:
				g.TouchEvent(e.X, e.Y, e.Type)
			}
		}
	})
}
