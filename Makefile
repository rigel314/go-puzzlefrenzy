all: android

ifeq (${ANDROID_NDK_HOME},)
ANDROID_NDK_HOME := "C:/Users/cody/source/android-ndk-r25b"
endif

android:
	gomobile build -target android -o go-puzzlefrenzy.apx -x ./target/android

.PHONY: all android
