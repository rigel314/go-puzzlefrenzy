package main

import (
	"flag"
	"image/png"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

type data struct {
	Name   string
	Width  int
	Height int
	Pixels []pxls
}

type pxls struct {
	X, Y       int
	R, G, B, A uint8
}

func main() {
	flag.Parse()

	dir := flag.Arg(0)
	output := flag.Arg(1)

	t := template.Must(template.New("spritegen").Parse(tmpl))

	files, err := os.ReadDir(dir)
	if err != nil {
		panic(err)
	}

	var tmpldata []data

	for _, v := range files {
		f, err := os.Open(filepath.Join(dir, v.Name()))
		if err != nil {
			panic(err)
		}
		img, err := png.Decode(f)
		if err != nil {
			panic(err)
		}

		d := data{
			Name:   fixname(v.Name()),
			Width:  img.Bounds().Dx(),
			Height: img.Bounds().Dy(),
		}

		for j := 0; j < img.Bounds().Dy(); j++ {
			for i := 0; i < img.Bounds().Dx(); i++ {
				if r, g, b, a := img.At(i, j).RGBA(); a != 0 {
					// fmt.Printf("X")
					d.Pixels = append(d.Pixels, pxls{
						X: i, Y: j,
						R: uint8(r / 256), G: uint8(g / 256), B: uint8(b / 256), A: uint8(a / 256),
					})
				} else {
					// fmt.Printf(" ")
					_ = 0
				}
			}
			// fmt.Printf("\n")
		}
		// fmt.Printf("\n")

		tmpldata = append(tmpldata, d)
	}

	out, err := os.Create(output)
	if err != nil {
		panic(err)
	}

	err = t.Execute(out, tmpldata)
	if err != nil {
		panic(err)
	}
}

func fixname(s string) string {
	for i := len(s) - 1; i >= 0; i-- {
		if s[i] == '.' {
			s = s[:i]
			break
		}
	}

	s = strings.ToUpper(s[0:1]) + s[1:]

	return s
}

var tmpl = `package game

import (
	"image"
	"image/color"
)

var (
	{{- range . }}
	{{ .Name }}Image image.Image
	{{- end }}
)

{{ range . -}}
func init() {
	{{ .Name }}Image = image.NewRGBA(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{ {{- .Width }}, {{ .Height }}},
	})

	{{ $name := .Name }}
	{{- $name }}ImageReal := {{ $name }}Image.(*image.RGBA)
	{{ range .Pixels }}
	{{ $name }}ImageReal.Set({{ .X }}, {{ .Y }}, color.RGBA{ {{- .R }}, {{ .G }}, {{ .B }}, {{ .A }}})
	{{- end }}
}
{{ end -}}
`
