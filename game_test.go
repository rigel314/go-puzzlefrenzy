package game

import (
	"testing"

	"gitlab.com/rigel314/go-puzzlefrenzy/engine"
	"golang.org/x/mobile/gl"
)

func TestGameInit(t *testing.T) {
	g := new(Game)

	g.InitContext(&testGlCtx{})
	g.Paint()

	for _, v := range g.objs {
		t.Log(v.(*engine.Sprite).Pos)
		for _, v := range v.(*engine.Sprite).Pixels {
			t.Log("\t", v.Pos)
		}
	}
}

type testGlCtx struct{}

func (glctx *testGlCtx) ActiveTexture(texture gl.Enum) {

}
func (glctx *testGlCtx) AttachShader(p gl.Program, s gl.Shader) {

}
func (glctx *testGlCtx) BindAttribLocation(p gl.Program, a gl.Attrib, name string) {

}
func (glctx *testGlCtx) BindBuffer(target gl.Enum, b gl.Buffer) {

}
func (glctx *testGlCtx) BindFramebuffer(target gl.Enum, fb gl.Framebuffer) {

}
func (glctx *testGlCtx) BindRenderbuffer(target gl.Enum, rb gl.Renderbuffer) {

}
func (glctx *testGlCtx) BindTexture(target gl.Enum, t gl.Texture) {

}
func (glctx *testGlCtx) BindVertexArray(rb gl.VertexArray) {

}
func (glctx *testGlCtx) BlendColor(red, green, blue, alpha float32) {

}
func (glctx *testGlCtx) BlendEquation(mode gl.Enum) {

}
func (glctx *testGlCtx) BlendEquationSeparate(modeRGB, modeAlpha gl.Enum) {

}
func (glctx *testGlCtx) BlendFunc(sfactor, dfactor gl.Enum) {

}
func (glctx *testGlCtx) BlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha gl.Enum) {

}
func (glctx *testGlCtx) BufferData(target gl.Enum, src []byte, usage gl.Enum) {

}
func (glctx *testGlCtx) BufferInit(target gl.Enum, size int, usage gl.Enum) {

}
func (glctx *testGlCtx) BufferSubData(target gl.Enum, offset int, data []byte) {

}
func (glctx *testGlCtx) CheckFramebufferStatus(target gl.Enum) gl.Enum {
	return target
}
func (glctx *testGlCtx) Clear(mask gl.Enum) {

}
func (glctx *testGlCtx) ClearColor(red, green, blue, alpha float32) {

}
func (glctx *testGlCtx) ClearDepthf(d float32) {

}
func (glctx *testGlCtx) ClearStencil(s int) {

}
func (glctx *testGlCtx) ColorMask(red, green, blue, alpha bool) {

}
func (glctx *testGlCtx) CompileShader(s gl.Shader) {

}
func (glctx *testGlCtx) CompressedTexImage2D(target gl.Enum, level int, internalformat gl.Enum, width, height, border int, data []byte) {

}
func (glctx *testGlCtx) CompressedTexSubImage2D(target gl.Enum, level, xoffset, yoffset, width, height int, format gl.Enum, data []byte) {

}
func (glctx *testGlCtx) CopyTexImage2D(target gl.Enum, level int, internalformat gl.Enum, x, y, width, height, border int) {

}
func (glctx *testGlCtx) CopyTexSubImage2D(target gl.Enum, level, xoffset, yoffset, x, y, width, height int) {

}
func (glctx *testGlCtx) CreateBuffer() gl.Buffer {
	return gl.Buffer{}
}
func (glctx *testGlCtx) CreateFramebuffer() gl.Framebuffer {
	return gl.Framebuffer{}
}
func (glctx *testGlCtx) CreateProgram() gl.Program {
	return gl.Program{Value: 1}
}
func (glctx *testGlCtx) CreateRenderbuffer() gl.Renderbuffer {
	return gl.Renderbuffer{}
}
func (glctx *testGlCtx) CreateShader(ty gl.Enum) gl.Shader {
	return gl.Shader{Value: 1}
}
func (glctx *testGlCtx) CreateTexture() gl.Texture {
	return gl.Texture{}
}
func (glctx *testGlCtx) CreateVertexArray() gl.VertexArray {
	return gl.VertexArray{}
}
func (glctx *testGlCtx) CullFace(mode gl.Enum) {

}
func (glctx *testGlCtx) DeleteBuffer(v gl.Buffer) {

}
func (glctx *testGlCtx) DeleteFramebuffer(v gl.Framebuffer) {

}
func (glctx *testGlCtx) DeleteProgram(p gl.Program) {

}
func (glctx *testGlCtx) DeleteRenderbuffer(v gl.Renderbuffer) {

}
func (glctx *testGlCtx) DeleteShader(s gl.Shader) {

}
func (glctx *testGlCtx) DeleteTexture(v gl.Texture) {

}
func (glctx *testGlCtx) DeleteVertexArray(v gl.VertexArray) {

}
func (glctx *testGlCtx) DepthFunc(fn gl.Enum) {

}
func (glctx *testGlCtx) DepthMask(flag bool) {

}
func (glctx *testGlCtx) DepthRangef(n, f float32) {

}
func (glctx *testGlCtx) DetachShader(p gl.Program, s gl.Shader) {

}
func (glctx *testGlCtx) Disable(cap gl.Enum) {

}
func (glctx *testGlCtx) DisableVertexAttribArray(a gl.Attrib) {

}
func (glctx *testGlCtx) DrawArrays(mode gl.Enum, first, count int) {

}
func (glctx *testGlCtx) DrawElements(mode gl.Enum, count int, ty gl.Enum, offset int) {

}
func (glctx *testGlCtx) Enable(cap gl.Enum) {

}
func (glctx *testGlCtx) EnableVertexAttribArray(a gl.Attrib) {

}
func (glctx *testGlCtx) Finish() {

}
func (glctx *testGlCtx) Flush() {

}
func (glctx *testGlCtx) FramebufferRenderbuffer(target, attachment, rbTarget gl.Enum, rb gl.Renderbuffer) {

}
func (glctx *testGlCtx) FramebufferTexture2D(target, attachment, texTarget gl.Enum, t gl.Texture, level int) {

}
func (glctx *testGlCtx) FrontFace(mode gl.Enum) {

}
func (glctx *testGlCtx) GenerateMipmap(target gl.Enum) {

}
func (glctx *testGlCtx) GetActiveAttrib(p gl.Program, index uint32) (name string, size int, ty gl.Enum) {
	return
}
func (glctx *testGlCtx) GetActiveUniform(p gl.Program, index uint32) (name string, size int, ty gl.Enum) {
	return
}
func (glctx *testGlCtx) GetAttachedShaders(p gl.Program) []gl.Shader {
	return nil
}
func (glctx *testGlCtx) GetAttribLocation(p gl.Program, name string) gl.Attrib {
	return gl.Attrib{}
}
func (glctx *testGlCtx) GetBooleanv(dst []bool, pname gl.Enum) {

}
func (glctx *testGlCtx) GetFloatv(dst []float32, pname gl.Enum) {

}
func (glctx *testGlCtx) GetIntegerv(dst []int32, pname gl.Enum) {

}
func (glctx *testGlCtx) GetInteger(pname gl.Enum) int {
	return 0
}
func (glctx *testGlCtx) GetBufferParameteri(target, value gl.Enum) int {
	return 0
}
func (glctx *testGlCtx) GetError() gl.Enum {
	return gl.Enum(0)
}
func (glctx *testGlCtx) GetFramebufferAttachmentParameteri(target, attachment, pname gl.Enum) int {
	return 0
}
func (glctx *testGlCtx) GetProgrami(p gl.Program, pname gl.Enum) int {
	return 1
}
func (glctx *testGlCtx) GetProgramInfoLog(p gl.Program) string {
	return ""
}
func (glctx *testGlCtx) GetRenderbufferParameteri(target, pname gl.Enum) int {
	return 0
}
func (glctx *testGlCtx) GetShaderi(s gl.Shader, pname gl.Enum) int {
	return 1
}
func (glctx *testGlCtx) GetShaderInfoLog(s gl.Shader) string {
	return ""
}
func (glctx *testGlCtx) GetShaderPrecisionFormat(shadertype, precisiontype gl.Enum) (rangeLow, rangeHigh, precision int) {
	return
}
func (glctx *testGlCtx) GetShaderSource(s gl.Shader) string {
	return ""
}
func (glctx *testGlCtx) GetString(pname gl.Enum) string {
	return ""
}
func (glctx *testGlCtx) GetTexParameterfv(dst []float32, target, pname gl.Enum) {

}
func (glctx *testGlCtx) GetTexParameteriv(dst []int32, target, pname gl.Enum) {

}
func (glctx *testGlCtx) GetUniformfv(dst []float32, src gl.Uniform, p gl.Program) {

}
func (glctx *testGlCtx) GetUniformiv(dst []int32, src gl.Uniform, p gl.Program) {

}
func (glctx *testGlCtx) GetUniformLocation(p gl.Program, name string) gl.Uniform {
	return gl.Uniform{}
}
func (glctx *testGlCtx) GetVertexAttribf(src gl.Attrib, pname gl.Enum) float32 {
	return 0
}
func (glctx *testGlCtx) GetVertexAttribfv(dst []float32, src gl.Attrib, pname gl.Enum) {

}
func (glctx *testGlCtx) GetVertexAttribi(src gl.Attrib, pname gl.Enum) int32 {
	return 0
}
func (glctx *testGlCtx) GetVertexAttribiv(dst []int32, src gl.Attrib, pname gl.Enum) {

}
func (glctx *testGlCtx) Hint(target, mode gl.Enum) {

}
func (glctx *testGlCtx) IsBuffer(b gl.Buffer) bool {
	return false
}
func (glctx *testGlCtx) IsEnabled(cap gl.Enum) bool {
	return false
}
func (glctx *testGlCtx) IsFramebuffer(fb gl.Framebuffer) bool {
	return false
}
func (glctx *testGlCtx) IsProgram(p gl.Program) bool {
	return false
}
func (glctx *testGlCtx) IsRenderbuffer(rb gl.Renderbuffer) bool {
	return false
}
func (glctx *testGlCtx) IsShader(s gl.Shader) bool {
	return false
}
func (glctx *testGlCtx) IsTexture(t gl.Texture) bool {
	return false
}
func (glctx *testGlCtx) LineWidth(width float32) {

}
func (glctx *testGlCtx) LinkProgram(p gl.Program) {

}
func (glctx *testGlCtx) PixelStorei(pname gl.Enum, param int32) {

}
func (glctx *testGlCtx) PolygonOffset(factor, units float32) {

}
func (glctx *testGlCtx) ReadPixels(dst []byte, x, y, width, height int, format, ty gl.Enum) {

}
func (glctx *testGlCtx) ReleaseShaderCompiler() {

}
func (glctx *testGlCtx) RenderbufferStorage(target, internalFormat gl.Enum, width, height int) {

}
func (glctx *testGlCtx) SampleCoverage(value float32, invert bool) {

}
func (glctx *testGlCtx) Scissor(x, y, width, height int32) {

}
func (glctx *testGlCtx) ShaderSource(s gl.Shader, src string) {

}
func (glctx *testGlCtx) StencilFunc(fn gl.Enum, ref int, mask uint32) {

}
func (glctx *testGlCtx) StencilFuncSeparate(face, fn gl.Enum, ref int, mask uint32) {

}
func (glctx *testGlCtx) StencilMask(mask uint32) {

}
func (glctx *testGlCtx) StencilMaskSeparate(face gl.Enum, mask uint32) {

}
func (glctx *testGlCtx) StencilOp(fail, zfail, zpass gl.Enum) {

}
func (glctx *testGlCtx) StencilOpSeparate(face, sfail, dpfail, dppass gl.Enum) {

}
func (glctx *testGlCtx) TexImage2D(target gl.Enum, level int, internalFormat int, width, height int, format gl.Enum, ty gl.Enum, data []byte) {

}
func (glctx *testGlCtx) TexSubImage2D(target gl.Enum, level int, x, y, width, height int, format, ty gl.Enum, data []byte) {

}
func (glctx *testGlCtx) TexParameterf(target, pname gl.Enum, param float32) {

}
func (glctx *testGlCtx) TexParameterfv(target, pname gl.Enum, params []float32) {

}
func (glctx *testGlCtx) TexParameteri(target, pname gl.Enum, param int) {

}
func (glctx *testGlCtx) TexParameteriv(target, pname gl.Enum, params []int32) {

}
func (glctx *testGlCtx) Uniform1f(dst gl.Uniform, v float32) {

}
func (glctx *testGlCtx) Uniform1fv(dst gl.Uniform, src []float32) {

}
func (glctx *testGlCtx) Uniform1i(dst gl.Uniform, v int) {

}
func (glctx *testGlCtx) Uniform1iv(dst gl.Uniform, src []int32) {

}
func (glctx *testGlCtx) Uniform2f(dst gl.Uniform, v0, v1 float32) {

}
func (glctx *testGlCtx) Uniform2fv(dst gl.Uniform, src []float32) {

}
func (glctx *testGlCtx) Uniform2i(dst gl.Uniform, v0, v1 int) {

}
func (glctx *testGlCtx) Uniform2iv(dst gl.Uniform, src []int32) {

}
func (glctx *testGlCtx) Uniform3f(dst gl.Uniform, v0, v1, v2 float32) {

}
func (glctx *testGlCtx) Uniform3fv(dst gl.Uniform, src []float32) {

}
func (glctx *testGlCtx) Uniform3i(dst gl.Uniform, v0, v1, v2 int32) {

}
func (glctx *testGlCtx) Uniform3iv(dst gl.Uniform, src []int32) {

}
func (glctx *testGlCtx) Uniform4f(dst gl.Uniform, v0, v1, v2, v3 float32) {

}
func (glctx *testGlCtx) Uniform4fv(dst gl.Uniform, src []float32) {

}
func (glctx *testGlCtx) Uniform4i(dst gl.Uniform, v0, v1, v2, v3 int32) {

}
func (glctx *testGlCtx) Uniform4iv(dst gl.Uniform, src []int32) {

}
func (glctx *testGlCtx) UniformMatrix2fv(dst gl.Uniform, src []float32) {

}
func (glctx *testGlCtx) UniformMatrix3fv(dst gl.Uniform, src []float32) {

}
func (glctx *testGlCtx) UniformMatrix4fv(dst gl.Uniform, src []float32) {

}
func (glctx *testGlCtx) UseProgram(p gl.Program) {

}
func (glctx *testGlCtx) ValidateProgram(p gl.Program) {

}
func (glctx *testGlCtx) VertexAttrib1f(dst gl.Attrib, x float32) {

}
func (glctx *testGlCtx) VertexAttrib1fv(dst gl.Attrib, src []float32) {

}
func (glctx *testGlCtx) VertexAttrib2f(dst gl.Attrib, x, y float32) {

}
func (glctx *testGlCtx) VertexAttrib2fv(dst gl.Attrib, src []float32) {

}
func (glctx *testGlCtx) VertexAttrib3f(dst gl.Attrib, x, y, z float32) {

}
func (glctx *testGlCtx) VertexAttrib3fv(dst gl.Attrib, src []float32) {

}
func (glctx *testGlCtx) VertexAttrib4f(dst gl.Attrib, x, y, z, w float32) {

}
func (glctx *testGlCtx) VertexAttrib4fv(dst gl.Attrib, src []float32) {

}
func (glctx *testGlCtx) VertexAttribPointer(dst gl.Attrib, size int, ty gl.Enum, normalized bool, stride, offset int) {

}
func (glctx *testGlCtx) Viewport(x, y, width, height int) {

}
