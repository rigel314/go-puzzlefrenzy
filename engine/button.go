package engine

import (
	"image"
	"image/color"
	"image/draw"
	"log"

	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/geom"
	"golang.org/x/mobile/gl"
)

// Button holds the state for a 2D image with an associated function
type Button struct {
	Pos XY

	images     *glutil.Images
	fn         func() error
	m, mtouch  *glutil.Image
	sz         size.Event
	imgW, imgH int
	txt        string
}

var _ = Drawer(&Button{})
var _ = Toucher(&Button{})

// NewButton initializes the openGL parts of a Button
func NewButton(ctx gl.Context, txt string, fn func() error) *Button {
	o := Button{}

	o.txt = txt

	o.images = glutil.NewImages(ctx)

	o.imgW, o.imgH = len(txt)*(fontWidth+1)+1, fontHeight+2
	o.m = o.images.NewImage(o.imgW, o.imgH)
	o.mtouch = o.images.NewImage(o.imgW, o.imgH)
	draw.Draw(o.m.RGBA, o.m.RGBA.Bounds(), image.Black, image.Point{}, draw.Src)
	draw.Draw(o.mtouch.RGBA, o.mtouch.RGBA.Bounds(), image.White, image.Point{}, draw.Src)

	for i, c := range ([]byte)(txt) {
		glyph := glyphs[c]
		if len(glyph) != fontWidth*fontHeight {
			glyph = defaultGlyph
		}
		for y := 0; y < fontHeight; y++ {
			for x := 0; x < fontWidth; x++ {
				if glyph[fontWidth*y+x] == ' ' {
					continue
				}
				o.m.RGBA.SetRGBA((fontWidth+1)*i+x+1, y+1, color.RGBA{A: 0xff})
				o.mtouch.RGBA.SetRGBA((fontWidth+1)*i+x+1, y+1, color.RGBA{A: 0xff})
			}
		}
	}

	o.m.Upload()
	o.mtouch.Upload()

	return &o
}

// Draw runs the opengl instructions for drawing a Button
func (o *Button) Draw(touch XY) {
	img := o.m
	if pointInBounds(touch, o.Pos, o.posMax()) {
		img = o.mtouch
	}

	img.Draw(
		o.sz,
		geom.Point{X: geom.Pt(o.Pos.X / o.sz.PixelsPerPt), Y: geom.Pt(o.Pos.Y / o.sz.PixelsPerPt)},
		geom.Point{X: geom.Pt(o.Pos.X/o.sz.PixelsPerPt) + geom.Pt(o.imgW), Y: geom.Pt(o.Pos.Y / o.sz.PixelsPerPt)},
		geom.Point{X: geom.Pt(o.Pos.X / o.sz.PixelsPerPt), Y: geom.Pt(o.Pos.Y/o.sz.PixelsPerPt) + geom.Pt(o.imgH)},
		img.RGBA.Bounds(),
	)
}

// SetDimensions sets the physical screen dimensions for proper scaling
func (o *Button) SetDimensions(d XY, sz size.Event) {
	o.sz = sz
}

// Touch handles a touchEnd event for a Button
func (o *Button) Touch(touch XY) {
	if pointInBounds(touch, o.Pos, o.posMax()) {
		defer func() {
			if val := recover(); val != nil {
				log.Println("button touch event caught panic, label:", o.txt)
			}
		}()
		o.fn()
	}
}

func (o *Button) Cleanup() {
	o.m.Release()
	o.mtouch.Release()
}

func (o *Button) posMax() XY {
	return XY{
		X: o.Pos.X + geom.Pt(o.imgW).Px(o.sz.PixelsPerPt),
		Y: o.Pos.Y + geom.Pt(o.imgH).Px(o.sz.PixelsPerPt),
	}
}

func pointInBounds(point XY, min XY, max XY) bool {
	return point.X >= min.X && point.X <= max.X &&
		point.Y >= min.Y && point.Y <= max.Y
}

const (
	fontWidth  = 5
	fontHeight = 7
)

var defaultGlyph = "" +
	"X X X" +
	" X X " +
	"X X X" +
	" X X " +
	"X X X" +
	" X X " +
	"X X X"
var glyphs = [256]string{
	'0': "" +
		"  X  " +
		" X X " +
		"X   X" +
		"X   X" +
		"X   X" +
		" X X " +
		"  X  ",
	'1': "" +
		"  X  " +
		" XX  " +
		"X X  " +
		"  X  " +
		"  X  " +
		"  X  " +
		"XXXXX",
	'2': "" +
		" XXX " +
		"X   X" +
		"    X" +
		"  XX " +
		" X   " +
		"X    " +
		"XXXXX",
	'3': "" +
		"XXXXX" +
		"    X" +
		"   X " +
		"  XX " +
		"    X" +
		"X   X" +
		" XXX ",
	'4': "" +
		"   X " +
		"  XX " +
		" X X " +
		"X  X " +
		"XXXXX" +
		"   X " +
		"   X ",
	'5': "" +
		"XXXXX" +
		"X    " +
		"X XX " +
		"XX  X" +
		"    X" +
		"X   X" +
		" XXX ",
	'6': "" +
		"  XX " +
		" X   " +
		"X    " +
		"X XX " +
		"XX  X" +
		"X   X" +
		" XXX ",
	'7': "" +
		"XXXXX" +
		"    X" +
		"   X " +
		"   X " +
		"  X  " +
		" X   " +
		" X   ",
	'8': "" +
		" XXX " +
		"X   X" +
		"X   X" +
		" XXX " +
		"X   X" +
		"X   X" +
		" XXX ",
	'9': "" +
		" XXX " +
		"X   X" +
		"X  XX" +
		" XX X" +
		"    X" +
		"   X " +
		" XX  ",
	'E': "" +
		"XXXXX" +
		"X    " +
		"X    " +
		"XXXX " +
		"X    " +
		"X    " +
		"XXXXX",
	'F': "" +
		"XXXXX" +
		"X    " +
		"X    " +
		"XXXX " +
		"X    " +
		"X    " +
		"X    ",
	'L': "" +
		"X    " +
		"X    " +
		"X    " +
		"X    " +
		"X    " +
		"X    " +
		"XXXXX",
	'P': "" +
		"XXXX " +
		"X   X" +
		"X   X" +
		"XXXX " +
		"X    " +
		"X    " +
		"X    ",
	'S': "" +
		" XXX " +
		"X   X" +
		"X    " +
		" XXX " +
		"    X" +
		"X   X" +
		" XXX ",
	'V': "" +
		"X   X" +
		"X   X" +
		"X   X" +
		" X X " +
		" X X " +
		" X X " +
		"  X  ",
}
