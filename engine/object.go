package engine

import (
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/gl"
)

// XY holds an x,y pair of float32s
type XY struct {
	X, Y float32
}

// RGBA holds a color
type RGBA struct {
	R, G, B, A float32
}

// Drawer holds an Object or something that wraps an Object
type Drawer interface {
	Draw(touch XY)
	SetDimensions(d XY, sz size.Event)
	Cleanup()
}

type Toucher interface {
	Touch(touch XY)
}

// Object holds the state for a 2D object
type Object struct {
	Pos        XY
	Color      RGBA
	Scale      float32
	Dimensions XY
	ZIndex     float32

	glctx           gl.Context
	program         gl.Program
	shaderInput     gl.Attrib
	shaderColor     gl.Uniform
	shaderOffset    gl.Uniform
	shaderScale     gl.Uniform
	shaderDevScale  gl.Uniform
	shaderZIndex    gl.Uniform
	abuf            gl.Buffer
	drawMode        gl.Enum
	coordsPerVertex int
	vertexCount     int
}

var _ = Drawer(&Object{})

// NewObject initializes the openGL parts of an Object
func NewObject(ctx gl.Context, prg gl.Program, buf gl.Buffer, drawMode gl.Enum, coordsPerVertex, vertexCount int) *Object {
	o := Object{}

	o.glctx = ctx
	o.program = prg
	o.abuf = buf
	o.coordsPerVertex = coordsPerVertex
	o.vertexCount = vertexCount
	o.drawMode = drawMode

	o.shaderInput = o.glctx.GetAttribLocation(o.program, "inputVert")
	o.shaderOffset = o.glctx.GetUniformLocation(o.program, "offset")
	o.shaderColor = o.glctx.GetUniformLocation(o.program, "color")
	o.shaderScale = o.glctx.GetUniformLocation(o.program, "scale")
	o.shaderDevScale = o.glctx.GetUniformLocation(o.program, "devScale")
	o.shaderZIndex = o.glctx.GetUniformLocation(o.program, "zIndex")

	return &o
}

// Draw runs the opengl instructions for drawing an Object
func (o *Object) Draw(touch XY) {
	o.glctx.UseProgram(o.program)

	o.glctx.Uniform2f(o.shaderOffset, o.Pos.X/o.Dimensions.X, o.Pos.Y/o.Dimensions.Y)
	o.glctx.Uniform4f(o.shaderColor, o.Color.R, o.Color.G, o.Color.B, o.Color.A)
	o.glctx.Uniform1f(o.shaderScale, o.Scale)
	o.glctx.Uniform2f(o.shaderDevScale, 1, o.Dimensions.X/o.Dimensions.Y)
	o.glctx.Uniform1f(o.shaderZIndex, o.ZIndex)

	o.glctx.BindBuffer(gl.ARRAY_BUFFER, o.abuf)
	o.glctx.EnableVertexAttribArray(o.shaderInput)
	o.glctx.VertexAttribPointer(o.shaderInput, o.coordsPerVertex, gl.FLOAT, false, 0, 0)
	o.glctx.DrawArrays(o.drawMode, 0, o.vertexCount)

	o.glctx.DisableVertexAttribArray(o.shaderInput)
}

func (o *Object) Cleanup() {

}

// SetDimensions sets the physical screen dimensions for proper scaling
func (o *Object) SetDimensions(d XY, sz size.Event) {
	o.Dimensions = d
}
