package engine

import (
	"encoding/binary"
	"image"
	"sync"

	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/gl"
)

// Sprite holds the state for a 2D simple image
type Sprite struct {
	Pos    XY
	Scale  float32
	Pixels []Pixel
}

var _ = Drawer(&Sprite{})

type Pixel struct {
	RelativePos XY

	*Object
}

var lazyInit sync.Once
var program gl.Program
var squarebuf gl.Buffer

// NewSprite initializes the openGL parts of a Sprite
func NewSprite(ctx gl.Context, img image.Image) *Sprite {
	o := Sprite{}
	o.Scale = .01
	// o.Pos.X = 50
	// o.Pos.Y = 50

	lazyInit.Do(func() {
		var err error
		program, err = glutil.CreateProgram(ctx, vertexShader, fragmentShader)
		if err != nil {
			panic("hardcoded shaders shouldn't fail: " + err.Error())
		}
		squarebuf = ctx.CreateBuffer()
		ctx.BindBuffer(gl.ARRAY_BUFFER, squarebuf)
		ctx.BufferData(gl.ARRAY_BUFFER, squareData, gl.STATIC_DRAW)
	})

	r := img.Bounds()
	for i := 0; i < r.Dx(); i++ {
		for j := 0; j < r.Dy(); j++ {
			r, g, b, a := img.At(i, j).RGBA()

			if a == 0 {
				continue
			}

			pxl := NewObject(ctx, program, squarebuf, gl.TRIANGLE_STRIP, 3, 4)
			pxl.Color.R = float32(r) / 0xffff
			pxl.Color.G = float32(g) / 0xffff
			pxl.Color.B = float32(b) / 0xffff
			pxl.Color.A = float32(a) / 0xffff
			o.Pixels = append(o.Pixels, Pixel{
				Object:      pxl,
				RelativePos: XY{float32(i) * 5, float32(j) * 5},
			})
		}
	}

	return &o
}

// Draw runs the opengl instructions for drawing an Sprite
func (o *Sprite) Draw(touch XY) {
	for _, v := range o.Pixels {
		// recalculate position
		v.Pos.X = o.Pos.X + v.RelativePos.X
		v.Pos.Y = o.Pos.Y + v.RelativePos.Y
		v.Scale = o.Scale
		v.Draw(touch)
	}
}

// SetDimensions sets the physical screen dimensions for proper scaling
func (o *Sprite) SetDimensions(d XY, sz size.Event) {
	for _, v := range o.Pixels {
		v.Dimensions = d
	}
}

func (o *Sprite) Cleanup() {

}

var squareData = f32.Bytes(binary.LittleEndian,
	0.0, 1.0, 0.0, // top left
	1.0, 1.0, 0.0, // top right
	0.0, 0.0, 0.0, // bottom left
	1.0, 0.0, 0.0, // bottom right
)

const vertexShader = `
#version 100
uniform vec2 offset;
uniform float scale;
uniform vec2 devScale;
uniform float zIndex;

attribute vec4 inputVert;
void main() {
	// offset comes in with x/y values between 0 and 1.
	// inputVert bounds are -1 to 1.
	vec4 offset4 = vec4((2.0*offset.x-1.0), (1.0-2.0*offset.y), zIndex, 0);
	vec4 scale4 = vec4(scale*devScale.x, scale*devScale.y, 1, 1);
	gl_Position = inputVert*scale4 + offset4;
}`

const fragmentShader = `
#version 100
precision mediump float;
uniform vec4 color;
void main() {
	gl_FragColor = color;
}`
