package game

import (
	"log"
	"os"
	"path/filepath"

	"gitlab.com/rigel314/go-puzzlefrenzy/engine"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/gl"
)

//go:generate go run ./cmd/mksprite sprites/ sprites_gen.go

type Game struct {
	glctx gl.Context

	objs []engine.Drawer

	touchValid bool
	touch      engine.XY

	sz size.Event
}

// InitContext should be called to initialize a gl context, but not reset Game state
func (g *Game) InitContext(glctx gl.Context) {
	g.glctx = glctx

	// g.glctx.Enable(gl.DEPTH_TEST)

	// TODO: add various buttons on startup
	// TODO: handle buttons, and a level select screen
	// TODO: actual game logic, not just this display logic
}

// Paint should be called to draw a frame of the game
func (g *Game) Paint() {
	g.update()

	// g.glctx.ClearColor(0, 0, 0, 1)
	g.glctx.ClearColor(1, 1, 1, 1)
	g.glctx.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	for _, o := range g.objs {
		o.Draw(g.touch)
	}
}

// SizeEvent should be called when the window is resized or shortly after init
func (g *Game) SizeEvent(w, h int, sz size.Event) {
	widthPx := float32(w)
	heightPx := float32(h)

	g.sz = sz

	for _, o := range g.objs {
		o.SetDimensions(engine.XY{X: widthPx, Y: heightPx}, sz)
	}
}

// DestroyContext should be called to destroy the previously initialized context
func (g *Game) DestroyContext() {
}

// func drawborder(img *image.RGBA) {
// 	x, y := img.Rect.Min.X, img.Rect.Min.Y
// 	for i := 0; i < img.Rect.Dx(); i++ {
// 		img.Set(x+i, y, color.RGBA{0, 0, 0, 1})
// 		img.Set(x+i, y+img.Rect.Dy()-1, color.RGBA{0, 0, 0, 1})
// 	}
// 	for i := 0; i < img.Rect.Dy(); i++ {
// 		img.Set(x, y+i, color.RGBA{0, 0, 0, 1})
// 		img.Set(x+img.Rect.Dx()-1, y+i, color.RGBA{0, 0, 0, 1})
// 	}
// }

// var count = 0

func (g *Game) update() {
	// val := float32(.3)
	// for _, v := range g.objs {
	// 	if count%1000 < 500 {
	// 		v.(*engine.Sprite).Pos.X += val
	// 		v.(*engine.Sprite).Pos.Y += val
	// 	} else {
	// 		v.(*engine.Sprite).Pos.X -= val
	// 		v.(*engine.Sprite).Pos.Y -= val
	// 	}
	// }

	// count++
}

func (g *Game) TouchEvent(x, y float32, kind touch.Type) {
	g.touch = engine.XY{X: x, Y: y}
	switch kind {
	case touch.TypeBegin:
		g.touchValid = true
	case touch.TypeEnd:
		for _, v := range g.objs {
			if v, ok := v.(engine.Toucher); ok {
				v.Touch(g.touch)
			}
		}
		g.touchValid = false
	}
	if !g.touchValid {
		g.touch = engine.XY{X: -1, Y: -1}
	}
}

func rootConfigDir() string {
	filesDir := os.Getenv("FILESDIR")
	if filesDir == "" {
		log.Println("FILESDIR env was not set by android native code")
		return "/data/data" // probably won't work, but we can't make a better guess
	}

	return filepath.Join(filesDir, "fyne")
}
